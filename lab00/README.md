# Lab 0: Getting Started #

In this lab, we focus on getting setup with the tools that we will be using for the other labs.

1. ~~[Setting up Google Compute Engine for running our virtual machines](google-cloud.md)~~ In Fall 2021, this is already done for you.

  * To find your VM, go to the [Google Cloud list of VM instances](https://console.cloud.google.com/compute/instances?project=cs-491-591-intro-to-security) for our project CS 491-591.
    Your VM is called `introsec-YOURNAME` where `YOURNAME` is your PSU "Odin ID".  

  * Find your VM in the list and there you will find its public IP address.  (It should start with "34." or "35.")

  * From the terminal on your local device, you can SSH to your VM like this:
  
    ```console
    [user@vm]$ ssh YOURNAME@IP
    ```
  
    where `YOURNAME` is your PSU Odin ID and `IP` is the ip address of your VM.

2. [Generating SSH keys for secure access to remote resources](ssh-keys.md)

3. [Cloning and pushing Git repositories with Bitbucket](bitbucket-git.md)

