# C Programming: A Refresher #

This lab is intended as an easy (re)introduction to basic programming in the C language.

## Installing the Testing Tools ##

In order to run the automated test suite that we provide with this lab, you need to install some additional software from Ubuntu.

```console
user@host:lab01$ sudo apt install python3-pytest python3-pexpect
```

## The Guessing Game ##

The problems in this lab both revolve around a simple "guessing game" protocol.  The game has two players: the *oracle*, who knows a secret number, and the *player* who tries to guess the number.  Each time they play the game, the player only gets one guess.  A typical game goes something like this:

```
ORACLE: What is your name?
PLAYER: Bob
ORACLE: What is the magic number, Bob?
PLAYER: 123
ORACLE: TOO LOW
```

Your job in this assignment is to write two simple C programs, one to "speak" each side of the guessing game protocol.  The actual "wire" protocol here is quite simple:

* All messages must be sent and received via the programs' standard input (stdin) and standard output (stdout) as plan ASCII/UTF8 text.  (As a result, you can test your programs by running them on the command line and manually typing input into them.)
* All messages must match the given formats exactly.  For example, where the protocol says "What is your name?", if your program says "What's your name?" that will not count.
* All messages must be terminated with a carriage return character followed by a line feed character (ie, CRLF).  In C, this sequence can be written as "\r\n".

## Problem 1: The Oracle ##

Your first task is to write a C program to play the role of the Oracle.

Put your code in the file [oracle.c](oracle.c) in this subdirectory of the Git repo.

The job of the Oracle program is straightforward.  First the Oracle asks the Player for their name.  Then the Oracle prompts them for the magic number, using their name in the prompt.  Finally, the Oracle compares the guess to the correct answer and lets the Player know the result.

* If the Player's guess is less than the magic number, output "TOO LOW".
* If the Player's guess is greater than the magic number, output "TOO HIGH".
* If the Player's guess is correct, output "SUCCESS"

Hint: Don't forget the carriage return and line feed characters at the end of each line!

Correct answers for a small set of known users are given in the file [answers.txt](answers.txt).  Your Oracle program should consult this file when deciding whether the Player's guess is correct.

### Compiling your code ###

A Makefile has been provided to automate the process of compiling and testing your programs.  Running ```make``` with no arguments will compile the two programs.

```console
user@host:lab01$ make
gcc -o oracle oracle.c
gcc -o player player.c
```

Running ```make test``` runs the automated test suite with summary output.  See below for how to run the tests with more extensive, verbose output.

```console
user@host:lab01$ make test
```

### Turning in your Oracle code ###
Once you've got the Oracle program working reasonably well, go ahead and push it to your Git repository.  You can always push a newer version at any time, and you will be graded based on the latest commit before the deadline.

```console
user@host:lab01$ git add oracle.c
user@host:lab01$ git commit -m "Got the oracle working.  Yay!"
user@host:lab01$ git push personal master
```

**Hint:** Every time you finish a new feature or fix a bug, go ahead and push your latest version to git.  Then you'll always have a safe copy turned in, in case you have problems with your VM or your Internet connection, etc.

## Problem 2: The Player ##

The job of the Player program is also simple.  It takes one command-line argument: the name that it should use in the guessing game.  The Player program should get its guess from the file [guesses.txt](guesses.txt) in its current working directory.  The format of ```guesses.txt``` is the same as that of ```answers.txt``` above.

Put your code for the Player program in [player.c](player.c).

Once you have the Player program working reasonably well, go ahead and push it to Git too.

```console
user@host:lab01$ git add player.c
user@host:lab01$ git commit -m "Got the player working.  Yay!"
user@host:lab01$ git push personal master
```

## Running the Tests ##

An automated test suite is provided for you in the file ```tests.py```.  It uses Python's pytest framework.

You can use the Makefile to run the tests and get a summary of the results, like this:

```console
user@host:lab01$ make test
```

For more detailed output, you can run the tests directly using ```pytest``` like this:

```console
user@host:lab01$ pytest-3 -l -v
```



## Turn in Your Code! ##

When you're finished debugging and all the tests are passing -- or the deadline is looming and you're running out of time, heh -- either way, **please don't forget to turn in the latest version of your code**.

```console
user@host:lab01$ git add oracle.c player.c
user@host:lab01$ git commit -m "Hopefully the last commit for Lab 1"
user@host:lab01$ git push personal master
```

