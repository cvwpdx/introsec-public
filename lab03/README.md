# Lab 3: Smashing the Stack #

## Configuring Your VM

Before we can run classic exploit code on a modern system, we need to turn off the basic set of protections that have been put in place.  DEP and stack canaries can be disabled at compile time for each of our target programs.  But for ASLR, we need to modify one of the system-wide settings.  Just like we did at the beginning of [Lab 2](../lab02/README.md), we can disable ASLR with the following command:

```console
user@host:lab03$ sudo sysctl -w kernel.randomize_va_space=0
```

## Installing Pytest

This lab uses the `pytest` package for the automated test suite.  Pytest should already be installed on your VM, but just in case, you can run:

```console
user@host:lab03$ sudo apt install python3-pytest
```

## Pulling the latest skeleton code from introsec-public

Next, we need to fetch the latest updates from the [introsec-public](https://bitbucket.org/cvwpdx/introsec-public.git) Git repo.  This will pull in the baseline "skeleton" code for Lab 3.

Assuming you cloned from the `introsec-public` repo into a local folder called `homework`, you can pull from it using the identifier `origin` like this: 

```console
user@host:~$ cd homework
user@host:homework$ git pull origin master
```

As always for this course, we'll be working on the `master` branch.

## Compiling the Baseline Code

The baseline code for Lab 3 comes with a Makefile to automate the build process.  Type `make` to compile all the C programs, including the target programs and your attack code, with the proper set of compiler flags to disable DEP and StackGuard on the target programs.

```console
user@host:lab03$ make
```

## Running the Tests

Like [Lab 1](../lab01/README.md), this lab has a suite of automated tests.  The Makefile also provides a recipe for running the tests, so you don't have to remember the pytest syntax.

```console
user@host:lab03$ make test
```

The test suite dynamically generates a new flag file for each problem each time the test runs.  It then verifies that your program succeeds in retrieving the flag.


## Problem 1: Reverse Engineering with GDB (Part 1)

The program [target01](target01.c) asks you to guess a secret "magic" number. If you guess correctly, the program will spawn a shell for you.  Your "attack" program should use this shell to print out the contents of the file `flag01.txt` as the last line of its output.

_Hint_: On Linux, the `cat` program will read the contents of a file and print them to its standard output.

You have been provided with a copy of the program's source code in [target01.c](target01.c), but the magic number has been censored.  Use `gdb` to examine the program's memory as it runs.  Extract the magic value.

_Hint: Disassemble the code for the function `check_guess`.  It compares your input value to a hard-coded constant.  That constant is the magic number.  You can run the program under gdb using the command:_

```console
user@host:lab03$ gdb ./target01
```

Once you have reverse-engineered the target program to obtain the magic number, you can fill in [attack01.c](attack01.c) with a simple program to execute the "attack".  Your program should (1) feed the magic value into the program to obtain the shell, then (2) uses the shell to print the flag.  (It's OK, and normal, if you simply hard-code the magic number into your program.)

Your program should work when its output is piped in to the victim program's standard input, like this:

```console
user@host:lab03$ ./attack01 | ./target01
```

Alternatively, the Makefile includes a recipe for running this command.  The Makefile also knows to automatically re-compile your attack program if its source file has been modified since the last compile.

```console
user@host:lab03$ make run01
```

There is also a Makefile recipe for running the automated test just for Problem 1.

```console
user@host:lab03$ make test01
```

To receive credit for this problem, turn in your attack code in [attack01.c](attack01.c).


## Tips for Writing Your Attack Code

I received a very good question from a student who was working on Problem 1 and having trouble getting access to the shell from the attack program. When the student typed in the magic number manually, the shell spawned just as expected. But when they printed the same magic number from the attack program, the victim printed its "success" message, but no shell prompt appeared. What happened?

The problem happens because, unlike manual typing, your attack program can deliver all of its output *extremely* quickly. So it's easy for the victim program to slurp up all the bytes into some input buffer before the shell can even get started. If the shell starts and there's no input left to consume, it simply closes. We don't want that!

We need the victim program to get some of our output, and then we need the rest of our output to go to the shell.

The trick that I've found works the best is to use a combination of `fflush()` and `sleep()` to (1) make sure that all the data that we output for the victim program has actually been sent through the pipe, and then (2) give the new shell a second or two to start up. Then anything else we write should go to the shell. So, for example, our attack program might look something like this:

```c
  puts(attack_payload);  // This is the data that we want to go to the victim program
  fflush(stdout);        // Make sure all of the data above has made it through the pipe
  sleep(2);              // Wait for the shell to spawn...
  puts(shell_command);   // This is the data that we want to feed into the shell
```


## Problem 2: Reverse Engineering with GDB (Part 2)
The program in Problem 2 is very similar to the code from Problem 1. Only now the secret value is stored in the program's **data section**, rather than **on the stack**.

Once again, your task is to first run the victim program in GDB and extract its magic value.  Then you can re-run the program without gdb and feed it the magic number to obtain the shell.

Fill in [attack02.c](attack02.c) with a simple program to feed the "magic" value to the victim program and obtain the shell and print the flag, just like you did for Problem 2.  Your "attack" should cause the target program to print out the contents of the file `flag02.txt` as the last line of its output.

You can run this one just like you did for Problem 1.

```console
user@host:lab03$ make run02
```

Turn in your code in [attack02.c](attack02.c) when you are done.


## Problem 3: Buffer Overflow onto an Int
The program in Problem 3 stores an integer, `x`, on the stack in the vulnerable function `vuln()`. It assigns `x` the value 0. In order to obtain the flag, you must force `x` to take on a different value.

First analyze the program in `gdb` to extract the magic value for x that will cause the program to spawn a shell. Then craft an input that overflows the buffer `buf` and clobbers `x` with this magic value.  Your attack should then use the resulting shell to print out the contents of the file `flag03.txt` as the last line of its output.

Turn in your code in [attack03.c](attack03.c) when you are done.


## Problem 4: Buffer Overflow onto a Pointer
In Problem 4, the vulnerable program stores a function pointer on the stack in `vuln()`.

Your job is to craft an input that overflows the buffer `buf` to clobber the function pointer `func_ptr` in order to "steal" the flag as in the problems above.

_Hint: If `func_ptr` points to the function `happy()` when the program calls `func_ptr()`, what will happen?_

Turn in your code in [attack04.c](attack04.c) when you are done.


## Problem 5: Shell Code in C
In Problem 5, you begin the task of crafting a basic exploit payload for a code injection vulnerability.

Your job is to fill in the function `spawn_shell()` in [attack05.c](attack05.c) so that it invokes the `execve()` system call to spawn a shell. If the call to `execve()` fails, your code should cause the program to exit quietly.

_Hint_: It's tempting to simply re-use the C code given in lecture and used in Aleph One's "Smashing the Stack" paper. Please make sure you understand what the program is doing before you attempt to use it.   (This one should be relatively easy.)

Turn in your "attack" program [attack05.c](attack05.c). 


## Problem 6: Shell Code in Assembly
For Problem 6, you take one step closer to crafting your code injection exploit by re-writing your program from Problem 5 in x86 assembly code.

Your task is to fill in the function `spawn_shell()` in [attack06.s](attack06.s) so that it invokes the `execve` system call to spawn the shell. As in Problem 5 above, if the call to `execve` fails, your code should _exit_ quietly.

_Hint_: It's tempting to re-use assembly code from the lecture slides or from "Smashing the Stack". But beware! There are some typos in Smashing the Stack, and you will learn much more by writing this from scratch by yourself.  Also, there are some subtle assumptions underlying Aleph One's code that do not hold true when it runs as part of a "normal" program.  Write this one yourself as if you were simply writing a "normal" function in assembly, and you will have an easier time.  (Of course, feel free to use the "Smashing the Stack" code as a reference.)

Turn in your program in [attack06.s](attack06.s).


## Problem 7: Injecting a "Canned" Payload (Part 1)
The target program for Problem 7 reads in your machine code from `stdin`, sets a function pointer to it, and executes it. This gives you the opportunity to test out your shellcode and delivery mechanism without needing to guess the return address.

The skeleton code provided in [payload.h](payload.h) includes the pre-compiled binary shellcode from "Smashing the Stack."  Feel free to use it.  All you need to do is `include` the header file in your C code:

```C
#include "payload.h"
```

Turn in your completed "attack" program in [attack07.c](attack07.c). 


## Problem 8: Injecting a "Canned" Payload (Part 2)
The program in Problem 8 does not execute your code on its own. You must overflow its buffer and clobber the return address with an address that's somewhere in your nop sled.  Take control of `eip`, spawn a shell, and steal the flag!

Again, feel free to use Aleph One's pre-compiled payload in [payload.h](payload.h).

Turn in your completed "attack" program in [attack08.c](attack08.c). 


## BONUS Problem 9: Evading a Simple Filter
In Problem 9, the target program is on the lookout for your attack! Adapt your code from Problem 8 to evade its filter and steal the flag.

Turn in your completed "attack" program in [attack09.c](attack09.c). 


## Don't Forget to Turn In Your Code
Remember to run **git add** on all of your source files, then **git commit**, then finally **git push**.  If you have made any changes to your personal Git repo, you may need to **pull** them down into your local working copy before you can push any new changes back up into the repo.

```console
user@host:lab03$ git add attack*.c
user@host:lab03$ git commit -m "Done with Lab 3"
user@host:lab03$ git pull personal master
user@host:lab03$ git push personal master
```

