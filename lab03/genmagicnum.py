#!/bin/env python3

import os
import sys
import string
import random

# Different random seeds for each user
username = os.environ['USER']
if 'INTROSEC_USER' in os.environ:
    username = os.environ['INTROSEC_USER']

# Different random seeds for each use case
seed_string = username
if len(sys.argv) > 1:
    seed_string += sys.argv[-1]

random.seed(seed_string)

avoid_terminators = "target03.c" in sys.argv

if avoid_terminators:
    magic_bytes = []
    for i in range(4):
        byte_val = random.randint(0x21,0xFF)
        magic_bytes.append(byte_val)
    magic_bytes = tuple(magic_bytes)
    print("0x%02x%02x%02x%02x" % magic_bytes)
else:
    magic = random.randint(0,2**31-1)
    print("0x%08x" % magic)
