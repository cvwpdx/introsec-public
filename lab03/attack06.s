/*
 * Attack 07 - Assembly code to spawn a shell
 */
    .global main

    .text
spawn_shell:
    #
    # Your code goes here  
    #
    ret
main:
    # Main doesn't do much.
    # Just call spawn_shell.
    call    spawn_shell
    # Then, if by some accident we're still here, 
    # we return to the C runtime to exit the program.
    ret
